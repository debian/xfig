From: Thomas Loimer <thomas.loimer@tuwien.ac.at>
Date: Wed, 1 Jan 2025 21:25:24 +0100
Origin: upstream, https://sourceforge.net/p/mcj/xfig/ci/6cbbec8
Bug: https://sourceforge.net/p/mcj/tickets/182/
Forwarded: not-needed
Subject: Delete a text object with an empty string, #182
 Editing a text object and deleting the entire string is now equivalent to
 deleting the text object.

--- a/src/e_edit.c
+++ b/src/e_edit.c
@@ -3,7 +3,7 @@
  * Copyright (c) 1985-1988 by Supoj Sutanthavibul
  * Parts Copyright (c) 1989-2015 by Brian V. Smith
  * Parts Copyright (c) 1991 by Paul King
- * Parts Copyright (c) 2016-2024 by Thomas Loimer
+ * Parts Copyright (c) 2016-2025 by Thomas Loimer
  *
  * Change function implemented by Frank Schmuck (schmuck@svax.cs.cornell.edu)
  * X version by Jon Tombs <jon@uk.ac.oxford.robots>
@@ -2639,7 +2639,8 @@ get_new_text_values(void)
 	new_t->comments = conv_utf8strdup(panel_get_value(comments_panel));
 	font_setbbox_offset(new_t);
 	/* now set the font for this zoom scale */
-	font_reload(new_t);
+	if (new_t->cstring && *new_t->cstring)
+		font_reload(new_t);
 }
 
 static void
@@ -2665,6 +2666,13 @@ done_text(void)
 			add_depth(O_TXT, new_t->depth);
 		}
 		redisplay_texts(new_t, old_t);
+		/* an empty string effectively deletes the text object */
+		if (!new_t->cstring || *new_t->cstring == '\0') {
+			cur_t = new_t;
+			delete_text(new_t);
+			set_latesttext(old_t);
+			break;
+		}
 		clean_up();
 		old_t->next = new_t;
 		set_latesttext(old_t);
--- a/src/f_read.c
+++ b/src/f_read.c
@@ -1343,8 +1343,9 @@ static F_text  *
 read_textobject(FILE *fp)
 {
 	F_text	*t;
-	int	l,n,len;
+	int	n;
 	int	ignore = 0;
+	size_t	len;
 	char	s[BUF_SIZE], s_temp[BUF_SIZE], junk[2];
 	float	tx_size;
 	float	length, height;
@@ -1371,7 +1372,6 @@ read_textobject(FILE *fp)
 	 * NOTE: The height, length and descent will be recalculated from the
 	 *       actual font structure in read_scale_text().
 	 */
-
 	if (proto >= 30) {  /* order of parms is more like other objects now;
 			       string is now terminated with the literal '\001',
 			       and 8-bit characters are represented as \xxx */
@@ -1386,10 +1386,10 @@ read_textobject(FILE *fp)
 				&height, &length, &t->base_x, &t->base_y, s,
 				junk);
 	}
-	/* remove newline */
-	buf[strlen(buf) - 1] = '\0';
-	if (buf[strlen(buf) - 1] == '\r')
-		buf[strlen(buf) - 1] = '\0';
+	/* remove newline, if it is a multi-line string */
+	buf[(len = strlen(buf)) - 1] = '\0';
+	if (buf[len - 2] == '\r')
+		buf[len - 2] = '\0';
 	/* remove any trailing carriage returns (^M, possibly from a PC) */
 	if (s[strlen(s) - 1] == '\r')
 		s[strlen(s) - 1] = '\0';
@@ -1492,6 +1492,7 @@ read_textobject(FILE *fp)
 		/* now convert any \xxx to ascii characters */
 		if (strchr(s,'\\')) {
 			unsigned num;
+			size_t	l;
 			len = strlen(s);
 			for (l=0,n=0; l < len; l++) {
 				if (s[l]=='\\') {
@@ -1532,7 +1533,7 @@ read_textobject(FILE *fp)
 		t->cstring = strdup(s + 1);
 	else
 		t->cstring = conv_utf8strdup(s + 1);
-	if (t->cstring == NULL)
+	if (t->cstring == NULL || *t->cstring == '\0')
 		free_return(t);
 
 	if (!update_figs) {
--- a/src/u_draw.c
+++ b/src/u_draw.c
@@ -3,7 +3,7 @@
  * Copyright (c) 1985-1988 by Supoj Sutanthavibul
  * Parts Copyright (c) 1989-2015 by Brian V. Smith
  * Parts Copyright (c) 1991 by Paul King
- * Parts Copyright (c) 2016-2024 by Thomas Loimer
+ * Parts Copyright (c) 2016-2025 by Thomas Loimer
  *
  * Parts Copyright (c) 1992 by James Tough
  * Parts Copyright (c) 1998 by Georg Stemmer
@@ -1325,6 +1325,9 @@ void draw_text(F_text *text, int op)
     int		    ox, oy;
     int		    xmin, ymin, xmax, ymax;
 
+    if (text->cstring == NULL || text->cstring[0] == '\0')
+	return;
+
     text_bound(text, &xmin, &ymin, &xmax, &ymax);
     if (!overlapping(ZOOMX(xmin), ZOOMY(ymin), ZOOMX(xmax), ZOOMY(ymax),
 		     clip_xmin, clip_ymin, clip_xmax, clip_ymax))
--- a/src/u_fonts.c
+++ b/src/u_fonts.c
@@ -3,7 +3,7 @@
  * Copyright (c) 1985-1988 by Supoj Sutanthavibul
  * Parts Copyright (c) 1989-2015 by Brian V. Smith
  * Parts Copyright (c) 1991 by Paul King
- * Parts Copyright (c) 2016-2024 by Thomas Loimer
+ * Parts Copyright (c) 2016-2025 by Thomas Loimer
  *
  * Any party obtaining a copy of these files is granted, free of charge, a
  * full and unrestricted irrevocable, world-wide, paid up, royalty-free,
@@ -554,10 +554,8 @@ font_setbbox_offset(F_text *t)
 	XGlyphInfo		extents;
 	struct xfig_font	*xf_font;
 
-	len = strlen(t->cstring);
-
-	/* shortcut, nothing to do for an empty string */
-	if (len == 0) {
+	/* shortcut, nothing to do for a missing or empty string */
+	if (!t->cstring || (len = strlen(t->cstring)) == 0) {
 		t->length = 0;
 		t->bb[0].x = t->bb[1].x = t->base_x;
 		t->bb[0].y = t->bb[1].y = t->base_y;
